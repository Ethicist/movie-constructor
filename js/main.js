/*
new Swiper('.swiper-container', {
	loop: true,
	navigation: {
		nextEl: '.arrow',
	},
	breakpoints: {
		320: {
			slidesPerView: 1,
			spaceBetween: 20
		},
		541: {
			slidesPerView: 2,
			spaceBetween: 40
		}
	}
});

const menuButton = document.querySelector('.menu-button');
const menu = document.querySelector('.header');
menuButton.addEventListener('click', function () {
	menuButton.classList.toggle('menu-button-active');
	menu.classList.toggle('header-active');
})
*/

/*let data = [];
fetch('witcher/index.json').then(
  (response) => {
    data = response.json();
  }
);*/


const get_element = (tag_name, class_names, attributes) => {
  const element = document.createElement(tag_name);
  
  if(class_names) {
    element.classList.add(...class_names);
  }
  
  if(attributes) {
    for(const attribute in attributes) {
      element[attribute] = attributes[attribute];
    }
  }
  
  return element;
}

const create_header = (param) => {

  const header = get_element('header');
  const container = get_element('div', ['container']);
  const wrapper = get_element('div', ['header']);

  
  if(param.header.logo) {
    const logo = get_element('img', ['logo']);
    logo.src = param.header.logo;
    logo.attribure = 'Логотип ' + param.title;
    wrapper.append(logo);
  }
  
  if(param.header.menu) {
    const menu_wrapper = get_element('nav', ['menu-list']);
    
    const all_menuitems = param.header.menu.map(
      item => {
        const menu_link = get_element('a', ['menu-link']);
        menu_link.href = item.link;
        menu_link.textContent = item.title;
        return menu_link;
      }
    );
    
    //console.table(all_menuitems);
    menu_wrapper.append(...all_menuitems);
    wrapper.append(menu_wrapper);
  }
  
  if(param.header.social) {
    const social_wrapper = get_element('div', ['social']);
    
    const all_social = param.header.social.map(
      item => {
        const social_link = get_element('a', ['social-link']);
        social_link.append(
          get_element(
            'img',
            [],
            {
              src: item.image,
              alt: item.title
            }
          )
        );
        social_link.href = item.link;
        return social_link;
      }
    );
    
    //console.table(all_social);
    social_wrapper.append(...all_social);
    wrapper.append(social_wrapper);
  }
  
  header.append(container);
  container.append(wrapper);
  
  return header;
}

const movie_constructor = (selector, options) => {
  
  const app = document.querySelector(selector);
  app.classList.add('body-app');
  
  if (options) {
    app.append(create_header(options));
  }
  
}

movie_constructor(
  '.app',
  {
    "title": "Ведьмак",
    "header": {
      "logo": "witcher/logo.png",
      "social": [
        {
          "title": "Twitter",
          "link": "https://t.co/",
          "image": "witcher/social/twitter.svg"
        },
        {
          "title": "Instagram",
          "link": "https://instagram.com/",
          "image": "witcher/social/instagram.svg"
        },
        {
          "title": "Facebook",
          "link": "https://fb.com/",
          "image": "witcher/social/facebook.svg"
        },
      ],
      "menu": [
        {
          "title": "Описание",
          "link": "#description"
        },
        {
          "title": "Трейлер",
          "link": "#trailer"
        },
        {
          "title": "Отзывы",
          "link": "#reviews"
        }
      ]
    }
  }
);
